﻿/** Горбачёнок Дарья, БПИ192
Вариант 7. 7. Вычислить прямое произведение множеств А1, А2, …, Аn. Входные данные : целое положительное число n,
множества чисел А1, А2, …, Аn, мощности множеств равны между собой и мощность каждого множества больше или равна 1.
Количество потоков является входным параметром. **/

#include <iostream>
#include <thread>
#include <algorithm>
#include <string>
#include <vector>
#include "omp.h"

using namespace std;

// Осуществляет проверку ввода целого числа в заданных границах.
int int_input(string message, int min, int max)
{
	cout << message;
	string s;
	getline(cin, s);
	while (s.empty() || !all_of(s.begin(), s.end(), ::isdigit) || stoi(s) < min || stoi(s) > max)
	{
		cout << "Введено некорректное значение. Пожалуйста, повторите ввод в соответствии с ограничениями." << endl;
		cout << message;
		getline(cin, s);
	}
	return stoi(s);
}

// Генерирует множество множеств из чисел от 0 до 99.
vector<vector<int>> create_sets(int number_of_sets, int set_p)
{
	vector<vector<int>> result;
	vector<int> set;
	srand(time(NULL));
	for (int i = 0; i < number_of_sets; ++i)
	{
		for (int j = 0; j < set_p; ++j)
		{
			set.push_back(rand() % 100);
		}
		result.push_back(set);
		set.clear();
	}
	return result;
}

// Расставляет все числа из множества set на нужные места произведения result в позицию подмножества pos.
void place_set(vector<int> set, vector<vector<int>>& result, int result_p, int pos)
{
	int set_p = set.size();
	int number_of_el = result_p / (int)(pow(set_p, pos + 1));

	for (int i = 0; i < result_p / number_of_el; ++i)
	{
		for (int j = 0; j < number_of_el; ++j)
		{
			result[i * number_of_el + j][pos] = set[i % set_p];
		}
	}
}

// Выполняет расстановку по местам в результеате для нескольких входных множеств.
void place_sets(vector<vector<int>>& result, vector<vector<int>> sets, int result_p, int begin, int end)
{
	for (int i = begin; i < end; ++i)
	{
		place_set(sets[i], result, result_p, i);
	}
}

// Печатает множество из N множеств из P элементов в столбик.
void print_sets(vector<vector<int>> sets, int N, int P)
{
	cout << "{ {";
	for (int i = 0; i < N; ++i)
	{
		if (i != 0) cout << " {";
		for (int j = 0; j < P; ++j)
		{
			cout << sets[i][j];
			if (j != P - 1) cout << ", ";
		}
		if (i != N - 1) cout << "},\n";
	}
	cout << "} }\n";
}

// Находит прямое произведение множеств из р эл-ов используя заданное кол-во потоков
void find_cartesian_product(int num_of_sets, int p, int num_of_threads)
{
	// Мощность результата.
	int result_p = pow(p, num_of_sets);

	// Распределение между второстепенными потоками (сколько множеств на поток).
	int sets_for_thread = num_of_sets / num_of_threads;
	// Распределение главному потоку.
	int sets_for_main_thread = num_of_sets % num_of_threads;

	// Генерация множеств.
	vector<vector<int>> sets = create_sets(num_of_sets, p);
	cout << "Сгенерированное случайным образом множество множеств А1, А2, ..., Аn:" << endl;
	print_sets(sets, num_of_sets, p);

	// Количество потоков.
	omp_set_num_threads(num_of_threads);

	// Результат вычислений.
	vector<vector<int>> result(result_p, vector<int>(num_of_sets));

	// Потоки обрабатывают определенное количество множеств. Главный поток обрабатывает остаток.
# pragma omp parallel for default(none) shared (result, sets, res_len, sets_for_thread, thr_num, cout)
	for (int i = 0; i < num_of_threads; ++i)
	{
		place_sets(result, sets, result_p, sets_for_thread * i, sets_for_thread * (i + 1));
	}

	if (sets_for_main_thread != 0)
	{
		place_sets(result, sets, result_p, num_of_sets - sets_for_main_thread, num_of_sets);
	}

	// Вывод результата.
	cout << "\nРезультат нахождения декартового произведения заданных множеств:" << endl;
	print_sets(result, result_p, num_of_sets);
}

int main() {
	setlocale(LC_ALL, "Russian");

	int sets_num;
	int set_power;
	int thr_num;

	// Определение входных данных.
	sets_num = int_input("Задайте количество множеств (1 < N < 11): N = ", 2, 10);
	set_power = int_input("Задайте мощность множества (0 < P < 6): P = ", 1, 5);
	thr_num = int_input("Задайте количество используемых потоков (0 < K < " + to_string(sets_num + 1) + "): K = ", 1, sets_num);

	// Поиск декартового произведения заданных множеств.
	find_cartesian_product(sets_num, set_power, thr_num);

	return 0;
}