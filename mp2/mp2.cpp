﻿/*
Горбачёнок Дарья, БПИ192, Вариант 7
----------------------------------------------------------------------------------------------------------
Военная задача. Анчуария и Тарантерия – два крохотных латиноамериканских государства, затерянных
в южных Андах. Диктатор Анчуарии, дон Федерико, объявил войну диктатору Тарантерии, дону Эрнандо.
У обоих диктаторов очень мало солдат, но очень много снарядов для минометов, привезенных с последней
американской гуманитарной помощью. Поэтому армии обеих сторон просто обстреливают наугад территорию
противника, надеясь поразить что - нибудь ценное. Стрельба ведется по очереди до тех пор, пока  либо
не будут уничтожены все цели, либо стоимость потраченных снарядов не превысит суммарную стоимость
всего того, что ими можно уничтожить.Создать многопоточное приложение, моделирующее военные действия.
*/

#include <iostream>
#include <vector>
#include <thread>
#include <mutex>

using namespace std;

// Описывает некоторый объект в стране.
class CountryObject {
private:
	// Ценность объекта.
	int value;

public:
	CountryObject() {
		// от 0 до 99
		value = rand() % 100;
	}

	int getValue() {
		return value;
	}
};

// Описывает страну.
class Country {
private:
	// Название страны
	string name = "";
	// Набор объектов страны.
	vector<CountryObject> objects;
	// Количество объектов (задастся случайно).
	int num_of_objects;
	// Суммарная ценность всех объектов.
	int objects_value;
	// Стоимость снаряда.
	int weapon_cost;
	// Затраты на войну.
	int expenses;


public:
	Country(const string& new_name) {
		name = new_name;
		// Количество объектов в стране задается случайно.
		num_of_objects = 10 + rand() % 40;
		for (int i = 0; i < num_of_objects; i++) {
			CountryObject country_object;
			objects.push_back(country_object);
			// Подсчитываем суммарную ценность объектов в стране.
			objects_value += country_object.getValue();
		}
		// Стоимость снаряда в стране задается случайно.
		weapon_cost = 10 + rand() % 100;
		// Изначально затраты нулевые.
		expenses = 0;
	}

	string getName() {
		return name;
	}

	vector<CountryObject> getObjects() {
		return objects;
	}

	int getNumOfObjects() {
		return num_of_objects;
	}

	void decreaseNumOfObjects() {
		num_of_objects--;
	}

	int getObjectsValue() {
		return objects_value;
	}

	int getWeaponCost() {
		return weapon_cost;
	}

	int getExpenses() {
		return expenses;
	}

	// Увеличивает затраты на заданную сумму.
	void addExpenses(int expense) {
		expenses += expense;
	}

	// Разрушает объект с индексом index в стране.
	void destroyObject(int index) {
		objects.erase(objects.begin() + index);
	}

	// Определяет целесообразность продолжения войны для страны attacking_country со страной defending_country.
	bool warMakesSense(Country attacking_country, Country defending_country) {
		// Война имеет смысл для страны, если ее затраты пока меньше суммарной ценности страны-противника.
		return (attacking_country.expenses < defending_country.getObjectsValue());
	}

	// Случайно выбирает некоторый объект в стране для атаки.
	int chooseObject(Country defending_country) {
		int n = defending_country.getNumOfObjects();
		int index = rand() % n;
		return index;
	}

	// Выводит начальную информацию о стране.
	void countryInfo() {
		cout << "У страны " << name << ":" << endl << num_of_objects << " объектов суммарной ценностью " << objects_value << " <3" << endl;
		cout << "неиссякаемый запас снарядов, каждый стоимостью " << weapon_cost << " <3" << endl;
		cout << "---------------------------------------------------------------" << endl;
	}
};

bool warMakesSense(Country attacking_country, Country defending_country) {
	// Война имеет смысл для страны, если ее затраты пока меньше суммарной ценности страны-противника.
	return (attacking_country.getExpenses() < defending_country.getObjectsValue());
}

// Продолжение войны.
bool war = true;
// Описывает процесс борьбы.
void attack(Country& attacking_country, Country& defending_country, mutex& m) {
	int index;
	// Соблюдаем синхронизацию.
	m.lock();
	// Стрельба ведется по очереди до тех пор, пока либо не будут уничтожены все цели, либо стоимость
	// потраченных снарядов не превысит суммарную стоимость всего того, что ими можно уничтожить. 
	// Т.е. метод вызывается двумя потоками.
	if (warMakesSense(attacking_country, defending_country) && defending_country.getNumOfObjects() > 0 && war) {

		// Выбирается объект для атаки.
		index = attacking_country.chooseObject(defending_country);
		// Расходы атакующей страны увеличиваются на стоимость одного снаряда.
		attacking_country.addExpenses(attacking_country.getWeaponCost());
		cout << attacking_country.getName() << " нанесла удар по объекту противника " << " №" << index << " ценностью " << defending_country.getObjects()[index].getValue() <<
			" <3. Суммарно потрачено " << attacking_country.getExpenses() << " <3" << endl << endl;
		// Разрушается соответствующий объект в атакуемой стране.
		defending_country.destroyObject(index);
		// Количество объектов уменьшается.
		defending_country.decreaseNumOfObjects();
		// Немножко ждем.
		this_thread::sleep_for(100ms);
	}

	if (!warMakesSense(attacking_country, defending_country))
	{
		cout << attacking_country.getName() << " прекращает стрельбу по противнику, поскольку затраты стали превышать потенциальные выгоды." << endl;
		cout << "Ура! Мир!" << endl;
		war = false;
		// Немножко ждем.
		this_thread::sleep_for(100ms);
	}

	if (defending_country.getNumOfObjects() < 1)
	{
		cout << "Страна " << defending_country.getName() << " потерпела поражение: все её объекты были разрушены страной " << attacking_country.getName() << endl;
		// Немножко ждем.
		war = false;
		this_thread::sleep_for(100ms);
	}

	// Снимаем лок (освобождаем для другого потока).
	m.unlock();
}

int main()
{
	srand(time(NULL));
	setlocale(LC_ALL, "Russian");

	cout << "Это симулятор войны между странами Анчуария и Тарантерия. Ценность объектов и снарядов измеряется в сердечках <3." << endl;
	cout << "Война продолжается либо до победы одного из государств, либо до перемирия" << endl << "(Наступает, когда одна из стран прекращает борьбу, решив, что война утратила выгоду)" << endl;
	cout << "---------------------------------------------------------------" << endl;

	Country country1("Анчуария");
	country1.countryInfo();
	Country country2("Тарантерия");
	country2.countryInfo();

	mutex m;
	while (country1.warMakesSense(country1, country2) && country2.warMakesSense(country2, country1) && country1.getNumOfObjects() > 0 && country2.getNumOfObjects() > 0) {
		// Запуск двух потоков для начала стрельбы.
		thread thread1(&attack, ref(country1), ref(country2), ref(m));
		thread thread2(&attack, ref(country2), ref(country1), ref(m));

		// Ждем, пока потоки закончат работу.
		thread1.join();
		thread2.join();
	}
}
