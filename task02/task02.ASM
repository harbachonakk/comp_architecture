; ВАРИАНТ 7. 
; Разработать программу, которая вводит одномерный массив A[N], формирует из индексов положительных элементов 
; массива A новый массив B и выводит его. Память под массивы может выделяться как статически, так и динамически.

format PE console
entry start
 
include 'win32a.inc'

;------------------------------------------------------
section '.data' data readable writable
 
        strVectorSize           db 'Please, enter vector size: ', 0
        strInvalidSize          db 'Invalid size of vector', 10, 0
        strVectorSize_          db 'The vector B will be created of %d entered elements', 10, 10, 0
        stEnterElement          db 'Vector A element [%d] = ', 0
        strElementInput         db '%d', 0
        strVectorB              db 'Created vector B:', 10, 0
        strVectorBElements      db '[%d] = %d', 10, 0
 
        vector_size             dd 0
        min                     dd 0
        i                       dd ?
        tmp                     dd ?
        tmpStack                dd ?
        vec                     rd 100

;------------------------------------------------------
section '.code' code readable executable
start:
; 1) Vector A Input
        call VectorInput
        getVectorEnd:
 
; 2) Finish size Output
        push [vector_size]
        push strVectorSize_
        call [printf]

; 3) Vector B Output
        push strVectorB
        call [printf]
 
; 4) Vector B elements Output
        call VectorOutput
finish:
        call [getch]
 
        push 0
        call [ExitProcess]


VectorInput:
        push strVectorSize
        call [printf]
        add esp, 4
 
        push vector_size
        push strElementInput
        call [scanf]
        add esp, 8
 
        mov eax, [vector_size]
        cmp eax, 0
        jg  getVector

; Invalid vector size was entered
        push vector_size
        push strInvalidSize
        call [printf]
        call [getch]
        push 0
        call [ExitProcess]

; Else continue entering
getVector:
        xor ecx, ecx
        xor esi,esi  ; +; ecx = 0
        mov ebx, vec ; ebx = &vec
getVecLoop:
        mov [tmp], ebx
        cmp ecx, [vector_size]
 
        jge endInputVector
 
        ; Entering the element
        mov [i], ecx
        push ecx
        push stEnterElement
        call [printf]
        cmp ecx, 0
 
        add esp, 8
 
        push ebx
        push strElementInput
        call [scanf]
 
        ; Choosing positive values of elements
        cmp dword [ebx], 0
        jg addmi
           mov ecx, [i]
           inc ecx
 
        jmp getVecLoop
endInputVector:
        mov ebx, vec
        xor ecx, ecx
        mov [vector_size], esi
        jmp getVectorEnd


VectorOutput:
        mov [tmpStack], esp
        xor ecx, ecx            ; ecx = 0
        mov ebx, vec            ; ebx = &vec
putVecLoop:
        mov [tmp], ebx
        cmp ecx, [vector_size]
        je endOutputVector
        mov [i], ecx
 
        ; Element output
        push dword [ebx]
        push ecx
        push strVectorBElements
        call [printf]
 
        mov ecx, [i]
        inc ecx
        mov ebx, [tmp]
        add ebx, 4
        jmp putVecLoop
endOutputVector:
        mov esp, [tmpStack]
        ret
addmi:
        add esp, 8
        mov ecx, [i]
        inc ecx
        mov ebx, [tmp]
        dec ecx
        mov [ebx], ecx
        inc ecx
 
        add ebx, 4
        inc esi
        jmp getVecLoop

;------------------------------------------------------

section '.idata' import data readable
    library kernel, 'kernel32.dll',\
            msvcrt, 'msvcrt.dll',\
            user32,'USER32.DLL'
 
include 'api\user32.inc'
include 'api\kernel32.inc'
    import kernel,\
           ExitProcess, 'ExitProcess',\
           HeapCreate,'HeapCreate',\
           HeapAlloc,'HeapAlloc'
  include 'api\kernel32.inc'
    import msvcrt,\
           printf, 'printf',\
           scanf, 'scanf',\
           getch, '_getch'